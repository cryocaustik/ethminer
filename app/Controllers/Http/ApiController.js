'use strict'
const Env = use('Env')
const axios = require('axios')
const moment = require('moment')


class ApiController {
    constructor(){
        this.miner_id = '0x10A241d4BfBbf1484b524f5DcCAe1Dc1e8cc3D8b'
        this.url = `https://api.ethermine.org/miner/${this.miner_id}/`
    }

    index({ view }){
        return view.render('api.index')
    }

    async payouts(){
        let _url = this.url + 'payouts' 
        let d = await axios.get(_url)
            .then( res => {
                if(res.status != 200){
                    console.log(`error: status of ${res.status}`)
                    return NaN
                }
                return res.data.data
            })
            .catch( err => {
                console.log('error: ' + err)
                return NaN
            })
        
        return this.cleanPayouts(d)
    }

    cleanPayouts(data){
        try {
            let ethPrice = this.getEthValue()
            return ethPrice.then(d => {
                let clean = []
                let _eth_agg = 0
                let _usd_agg = 0
                let _usd_cur_agg = 0
                let b_unit = 1000000000000000000.0
                let cur_eth_ush = d[Object.keys(d).pop()]
                data.reverse()
                data.forEach( rcd => {
                    let _dt = moment.unix(rcd.paidOn).format('YYYY-MM-DD')
                    let _eth = rcd.amount/b_unit
                    let _usd = d[_dt]*_eth
                    let _usd_cur = cur_eth_ush*_eth
                    clean.push({
                        "paid_dt": _dt,
                        "eth": _eth,
                        "eth_agg": _eth_agg += _eth,
                        "usd": parseFloat(_usd.toFixed(2)),
                        "usd_cur": parseFloat(_usd_cur.toFixed(2)),
                        "usd_agg": parseFloat((_usd_agg += _usd).toFixed(2)),
                        "usd_cur_agg": parseFloat((_usd_cur_agg += _usd_cur).toFixed(2))
                    })
                })
                return clean
            })
        } catch (err) {
            console.log(err)
        }
    }

    async getEthValue(){
        try {
            let url = 'https://min-api.cryptocompare.com/data/histoday?fsym=ETH&tsym=USD&allData=1'
            let data = await axios.get(url)
                .then( res => {
                    if(res.status != 200){
                        console.log('getEthValue error:', res.status)
                        return NaN
                    }
                    return this.cleanEthData(res.data.Data)
                })
                .catch( err => {
                    console.log('getEthValue error: ' + err)
                    return NaN
                })
            
            return data
        } catch (err) {
            console.log('getEthValue error: ', err)
            return NaN
        }
    }

    cleanEthData(data){
        try {
            let priced = {}
            data.forEach(rcd => {
                let _dt = moment.unix(rcd.time).format('YYYY-MM-DD')
                let _val = [rcd.low, rcd.high].reduce((l,h) => {return l+h})/2
                priced[_dt] = _val
            })
            return priced
        } catch (err) {
            console.log('cleanEthDate error: ',err)
            return NaN
        }
    }
}

module.exports = ApiController
