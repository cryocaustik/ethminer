'use strict'

class EthMinerController {
    index({ view }){
        return view.render('home')
    }
    
    viz({ view }){
        return view.render('viz')
    }
}

module.exports = EthMinerController
