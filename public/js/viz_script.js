
$.get('/api/payouts', data => {
    let chart1 = c3.generate({
        bindto: '#chart1',
        data: {
            json: data,
            keys: {
                x: 'paid_dt',
                value: ['eth', 'usd']
            },
            names: {
                eth_agg: 'ETH',
                usd_agg: 'USD'
            },
            type: 'area-spline',
            axes: {
                eth: 'y',
                usd: 'y2'
            }
        },
        axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: '%Y-%m-%d'
                }
            },
            y2: {
                show: true
            }
        }
    });

    let chart2 = c3.generate({
        bindto: '#chart2',
        data: {
            json: data,
            keys: {
                x: 'paid_dt',
                value: ['eth_agg', 'usd_agg']
            },
            names: {
                eth_agg: 'ETH',
                usd_agg: 'USD'
            },
            type: 'area-spline',
            axes: {
                eth_agg: 'y',
                usd_agg: 'y2'
            }
        },
        axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: '%Y-%m-%d'
                }
            },
            y2: {
                show: true
            }
        }
    });

    let chart3 = c3.generate({
        bindto: '#chart3',
        data: {
            json: data,
            keys: {
                x: 'paid_dt',
                value: ['usd_cur_agg']
            },
            names: {
                usd_cur_agg: 'USD'
            },
            type: 'area-spline',
            axes: {
                eth_agg: 'y',
            }
        },
        axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: '%Y-%m-%d'
                }
            }
        }
    });

    document.querySelector('#totUSD').innerText = '$'+(data.pop().usd_cur_agg).toLocaleString()
    document.querySelector('#totETH').innerHTML += (data.pop().eth_agg).toFixed(4)
});

