# EthMiner ~ AdnoisJs Etheruem Mining Tracker
---

An AdonisJs site that uses the power of D3/C3 to visualize the stats of an EtherMiner CryptoMiner

## Setup 
---

- configure nginx proxypass on desired domain/subdomain
- create cron job to execute `adonis serve` on server boot 


## Dependencies 
---

- [AdonisJs](https://adonisjs.com/)
- [Bootstrap 4](https://getbootstrap.com)
    - [jQuery-3.3.1](https://jquery.com/download/)
- [C3js](https://c3js.org/)
    - [D3](https://d3js.org/)

